package model;

import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by gabriel on 03/05/17.
 */
public class User {

    private static final Pattern EMAIL_REGEXP =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private String email;
    private String password;
    private String passwordConfirmation;
    private String conditionsApproved;
    private String errorMessage;
    private Date creationDate;

    public User(String email, String password, String passwordConfirmation, String conditionsApproved) {
        this.email = email;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.conditionsApproved = conditionsApproved;
    }

    public String getEmail() {
        return email;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getConditionsApproved() {
        return conditionsApproved;
    }

    public Date getCreationDate() {
        return new Date();
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /***
     * Verify that every field has a correct value
     * @return true if everything is OK, else false
     */
    public boolean checkValues() {
        boolean result = true;
        if(!EMAIL_REGEXP.matcher(email).matches()) {
            errorMessage = "Email address is invalid";
            result =  false;
        }
        else if(password.length() < 8) {
            errorMessage = "Password's length is not sufficient";
            result = false;
        }
        else if(!password.equals(passwordConfirmation)) {
            errorMessage = "Passwords are not identical";
            result = false;
        }
        else if(conditionsApproved == null) {
            errorMessage = "Agreements not accepted";
            result = false;
        }
        return result;
    }
}
