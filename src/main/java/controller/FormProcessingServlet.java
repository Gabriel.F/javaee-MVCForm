package controller;

import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Created by gabriel on 30/04/17.
 */
@WebServlet("/processForm")
public class FormProcessingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        User user = new User(
                request.getParameter("email"),
                request.getParameter("password"),
                request.getParameter("passwordVerification"),
                request.getParameter("approvedCheckbox")
        );

        if(user.checkValues()) {
            user.setCreationDate(new Date());
            request.setAttribute("user", user);
            request.getRequestDispatcher("confirmation.jsp").forward(request, response);
        }
        else {
            request.setAttribute("user", user);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
