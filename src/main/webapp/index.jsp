<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Inscription Form</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
    <form action="processForm" method="post">
        <input type="text" value="${ not empty user ? user.email : ''}"
               placeholder="Email address" name="email" />
        <br/><input type="password" placeholder="Password" name="password" />
        <br/><input type="password" placeholder="Enter password again" name="passwordVerification" />

        <br/><br/>
        <label for="approvedCheckbox">J'ai lu et approuvé les conditions générales de ce site</label>
        <input type="checkbox" id="approvedCheckbox" name="approvedCheckbox"
               ${ not empty user && user.conditionsApproved eq 'on' ? 'checked' : ''} />

        <br/>
        <button type="submit">Envoyer</button>
    </form>

    <c:if test="${not empty user}">
        <span class="red">Error : ${user.errorMessage} </span>
    </c:if>
</body>
</html>
