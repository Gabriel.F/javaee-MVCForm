<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Confirmation message</title>
</head>
<body>
    <p>Votre inscription a bien été prise en compte le <fn:formatDate value="${user.creationDate}" pattern="dd/MM/YYYY à HH:mm"/>
        pour l'adresse mail ${user.email}</p>
</body>
</html>
